package org.hibernate.tutorial.hbm;

import java.io.File;

import junit.framework.TestCase;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

/**
 * Illustrates use of Hibernate native APIs.
 *
 * @author Afonso Rem�dios
 */
public class GenerateDDLFileTest extends TestCase {
	private static final String DDLFileName = "DDLFile.sql";
	
	private SessionFactory sessionFactory;

	@Override
	protected void setUp() throws Exception {
		
		File ddlGeneratedFile = new File(DDLFileName);
		ddlGeneratedFile.delete();//false if didn't delete, true if deleted
		
	}

	@Override
	protected void tearDown() throws Exception {
	}

	public void testBasicUsage() {
		
		
		// A SessionFactory is set up once for an application
		Configuration hibConf = new Configuration();
		hibConf.configure(); // configures settings from hibernate.cfg.xml
        
		
		SchemaExport schemaExport = new SchemaExport(hibConf);
        schemaExport.setOutputFile(DDLFileName);
		
        schemaExport.execute(true, true, false, true);
        
//        schemaExport.drop(true, true);
        
		File ddlGeneratedFile = new File(DDLFileName);
		
		assertNotNull(ddlGeneratedFile);
		
		assertTrue("File was not generated.",ddlGeneratedFile.exists());
		
		
		
		
	}
}
